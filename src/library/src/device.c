/*
 * device.c
 * robarago@gmail.com
 * April, 2020
 * max7219mod library device handling helpers
 */
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/sysmacros.h>

#define TRY(c, m) if(-1 == c) { perror(m); exit(1); }

int max7219mod_dev;

unsigned int getMajor() {
	struct stat statbuf;
	TRY(fstat(max7219mod_dev, &statbuf),
		"Can't get dev_t using fstat - is module max7219m loaded?");
	return major(statbuf.st_rdev);
}

void max7219mod_set_anim_mode(unsigned int anim_mode) {
	TRY(ioctl(max7219mod_dev, _IOR(getMajor(), 3, int), anim_mode),
		"Can't set animation mode value with ioctl");
}

void max7219mod_set_intensity(unsigned int intensity) {
	TRY(ioctl(max7219mod_dev, _IOR(getMajor(), 2, int), intensity),
		"Can't set intensity value with ioctl");
}

void max7219mod_set_scroll_mode(unsigned int scroll_mode) {
	TRY(ioctl(max7219mod_dev, _IOR(getMajor(), 1, int), scroll_mode),
		"Can't set scroll_mode with ioctl");
}

void max7219mod_set_delay(unsigned int msdelay) {
	TRY(ioctl(max7219mod_dev, _IOR(getMajor(), 0, int), msdelay),
		"Can't set msdelay with ioctl");
}

void max7219mod_open(void) {
	max7219mod_dev = open("/dev/max7219m", O_WRONLY);
	TRY(-1 == max7219mod_dev, "Can't open max7219m device");
}

void max7219mod_close(void) {
	TRY(-1 == close(max7219mod_dev), "Can't close max7219m device");
}

void max7219mod_send_data(unsigned char* data, size_t length) {
	TRY(write(max7219mod_dev, data, sizeof(uint8_t) * length),
		"Can't write to max7219m device");
}
