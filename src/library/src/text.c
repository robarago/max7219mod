#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <zlib.h>
#include <stdbool.h>

#include "max7219m.h"

// Macros
#define MAX_BUFFER_LENGTH 4096
#define TRY(c, m, ...) if(c) { fprintf(stderr, m, ##__VA_ARGS__); exit(1); }

// externs
extern bool _debug;
unsigned char data[MAX_BUFFER_LENGTH];

// Common magic number of PSF
struct {
	unsigned char magic[2];
	unsigned char mode;
	unsigned char charsize;
} header1;
struct {
	unsigned char magic[4];
	unsigned int version;
	unsigned int headersize;
	unsigned int flags;
	unsigned int length;
	unsigned int charsize;
	unsigned int height, width;
} header2;
uint8_t* chars = NULL;

uint8_t charRow(const uint8_t ch, uint8_t offset) {
	return chars[ch * 8 + offset];
}

void binRepr(const uint8_t ch) {
	uint8_t i, j;
	uint8_t row;
	for(i = 0; i < 8; i++) {
		uint8_t row = charRow(ch, i);
		for(j = 0; j < 8; j++) {
			if(row & 0x80) printf("X");
			else printf(".");
			row = row << 1;
		}
		puts("");
	}
}

uint8_t nextChar(const unsigned char* string, uint8_t* p, uint8_t len) {
	if(*p < 0 || *p >= len) return 0xff;
	if(string[*p] > 128)
		return ((string[(*p)++] & 0x3f) << 6) | (string[(*p)++] & 0x3f);
	return string[(*p)++];
}

void max7219mod_load_font(const char* fontfile) {
	gzFile font = gzopen(fontfile, "r");
	TRY(NULL == font, "ERROR: Font '%s' not found or not readable\n", fontfile)
	gzread(font, &header1, sizeof(header1));
	if(header1.magic[0] == 0x36 && header1.magic[1] == 0x04) {
		TRY(header1.charsize > 8, "Height of font (%d) is invalid - Must be 8\n",
			header1.charsize)
		uint16_t nchars = header1.mode & 1 ? 512 : 256;
		chars = calloc(header1.charsize * nchars, sizeof(uint8_t));
		gzread(font, chars, header1.charsize * nchars);

		if(_debug) {
			printf("Using '%s' as valid PSF1 font\n", fontfile);
			printf("mode = %d\n", header1.mode);
			printf("charsize = %d\n", header1.charsize);
		}
		return;
	}
	gzseek(font, 0, SEEK_SET);
	gzread(font, &header2, sizeof(header2));
	TRY(header2.magic[0] != 0x72 || header2.magic[1] != 0xb5 ||
		header2.magic[2] != 0x4a || header2.magic[3] != 0x86,
		"ERROR: Unrecognized font format - Only PSF1 and PSF2 allowed\n")
	TRY(header2.height > 8 || header2.width > 8,
		"ERROR: Height or width of font (%d x %d) is invalid - Must be 8 x 8\n",
		header2.height, header2.width)
	uint16_t nchars = header2.length;
	chars = calloc(header2.charsize * nchars, sizeof(uint8_t));
	gzread(font, chars, header2.charsize * nchars);

	if(_debug) {
		printf("Using '%s' as valid PSF2 font\n", fontfile);
		printf("headersize = %d\n", header2.headersize);
		printf("flags = %d\n", header2.flags);
		printf("length = %d\n", header2.length);
		printf("charsize = %d\n", header2.charsize);
		printf("height = %d\n", header2.height);
		printf("width = %d\n", header2.width);
	}
}

// Render message in data array and send it to the Device
void max7219mod_send_string(unsigned char* string) {
	size_t i = 0;
	uint8_t ch, j, p = 0, len = strlen(string);

	TRY(chars == NULL, "ERROR: Can't render text without a font!\n");

	while(0xff != (ch = nextChar(string, &p, len)))
		for(j = 0; j < 8; j++)
			data[i++] = charRow(ch, j);
	max7219mod_send_data(data, i);
}
