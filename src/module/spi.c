/*
 * spi.c
 * robarago@gmail.com 2020
 * ARMv7 driver module for a MAX7219 matrix array - spi and array functions
 */
#include "max7219.h"
#include <linux/spi/spi.h>
#include <linux/dma-mapping.h>

extern int bus;
extern int intensity;
extern int test_value;
extern int array_count;

#define TRY(c, m, ...) if(c) { printk(m, ##__VA_ARGS__); return -ENODEV; }

#define DECODE_MODE    0x09
#define INTENSITY      0x0a
#define SCAN_LIMIT     0x0b
#define DISPLAY_TEST   0x0f
#define SHUTDOWN       0x0c

// Slave device
static struct spi_device *max7219_dev;
//static uint16_t txbuf[MAX_ARRAY_COUNT]; // 8 by default: allocate only once
static uint16_t* txbuf;
static dma_addr_t max7219_dma;

// TODO : sendOneFrame - put data to an 8 x array_count x uint16_t
//        buffer one only transferency

// reg=1, c[]: 01 v0 01 v1 01 v2 01 v3 01 v4
void sendOneRow(unsigned char reg, unsigned char* cols, size_t len) {
	register uint8_t i;
	struct spi_message m;
	struct spi_transfer t = {
		.tx_buf = (void*)txbuf,
		.tx_dma = max7219_dma,
		.len = sizeof(uint16_t) * array_count
	};

	if(len != 1) { // Faster
		for(i = 1; i <= array_count; i++)
			txbuf[i - 1] = cols[i - 1] << 8 | reg;
	} else memset16(txbuf, *cols << 8 | reg, sizeof(uint16_t) * array_count);

	spi_message_init(&m);
	m.is_dma_mapped = 1;
	spi_message_add_tail(&t, &m);
	spi_sync(max7219_dev, &m); 
	//spi_write(max7219_dev, txbuf, sizeof(uint16_t) * array_count);
}

static void sendToAll(unsigned char reg, unsigned char value) {
	sendOneRow(reg, &value, 1);
}

// len=1: 01 v0 01 v0..., 02 v0 02 v0..., ..., 08 v0 08 v0...
// len=8: 01 v0 01 v0..., 02 v1 02 v1..., ..., 08 v7 08 v7...
static void sendAllRows(unsigned char* values, size_t len) {
	register uint8_t i;
	for(i = 1; i <= 8; i++)
		sendToAll(i, len == 1 ? *values : values[i - 1]);
}

int initializeArray(int bus) {
	struct spi_master *master;

	// Register information about the SPI slave device
	struct spi_board_info spi_device_info = {
		.mode = SPI_MODE_0,
		.modalias = "max7219",
		.max_speed_hz = 5000000,
		.bus_num = bus,
		.chip_select = 0,
	};
	
	// Check /sys/class/spi_master for bus number availability in your system
	master = spi_busnum_to_master(spi_device_info.bus_num);
	TRY(!master, MALERT "MASTER not found.\n");

	// create a new slave device, given the master and device info
	max7219_dev = spi_new_device(master, &spi_device_info);
	TRY(!max7219_dev, MALERT "FAILED to create slave.\n");

	// Setup slave device
	max7219_dev->bits_per_word = 8;
	if(spi_setup(max7219_dev)) {
		printk(MALERT "FAILED to setup slave.\n");
	 	spi_unregister_device(max7219_dev);
		return -ENODEV;
	}

	// Allocate DMA resource
	max7219_dev->dev.coherent_dma_mask = ~0;
	// Memory allocated by this function will be automatically released on detach
	txbuf = (uint16_t*)dmam_alloc_coherent(&max7219_dev->dev,
		sizeof(uint16_t) * array_count, &max7219_dma, GFP_DMA);

	// Send initialization sequence to the MAX7219 controller
	sendToAll(SCAN_LIMIT, 7);	              // set up to scan all eight digits
	sendToAll(DECODE_MODE, 0);	            // Set BCD decode mode off
	sendToAll(DISPLAY_TEST, 0);	            // Disable test mode
	sendToAll(INTENSITY, intensity);        // set brightness 0 to 15
	sendAllRows((uint8_t *)&test_value, 1); // Set test pattern to value in param
	sendToAll(SHUTDOWN, 1);	                // come out of shutdown mode / turn on

	return 0;
}

void shutdownArray(void) {
	sendToAll(SHUTDOWN, 0);
	spi_unregister_device(max7219_dev);
}

void setArrayIntensity(unsigned int intensity) {
	sendToAll(INTENSITY, intensity);
}
