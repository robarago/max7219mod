/*
 * sendtext.c
 * robarago@gmail.com
 * April, 2020
 * ARMv7 user space text messenger for the MAX7219 array
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <max7219m.h>

const bool _debug = false;

// Macros
#define TRY(c, m, ...) if(c) { fprintf(stderr, m, ##__VA_ARGS__); exit(1); }

int main(int argc, char** argv) {
	TRY(argc != 6, "%s: %s <font-filename> <intensity> <scroll_mode> <msdelay> '<message>'\n",
		argv[0], argv[0])

	max7219mod_open();
	max7219mod_load_font(argv[1]);
	max7219mod_set_intensity(atoi(argv[2]));
	max7219mod_set_scroll_mode(atoi(argv[3]));
	max7219mod_set_delay(atoi(argv[4]));
	max7219mod_send_string(argv[5]);
	max7219mod_close();

	return 0;
}
