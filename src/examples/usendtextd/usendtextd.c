#include <microhttpd.h>
#include <max7219m.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#define MAIN_PAGE "\
<!doctype html>\n\
<html>\n\
	<head>\n\
		<meta charset=\"UTF-8\">\n\
		<meta http-equiv=\"Cache-Control\" content=\"no-store\" />\n\
		<title>MAX7219 array Messenger</title>\n\
		<style>\n\
			ul { padding: unset; }\n\
			form, li { display: flex; margin: 5px; }\n\
			input, select, button, label { flex-grow: 1; }\n\
			textarea { font-size: xxx-large; resize: vertical; }\n\
		</style>\n\
	</head>\n\
	<body>\n\
		<form name=\"msgform\" id=\"msgform\" action=\"/message\" method=\"post\">\n\
			<ul>\n\
				<li>\n\
					<label for=\"intensity\">Intensity</label>\n\
				</li>\n\
				<li>\n\
					<input required value=\"%d\" type=\"number\" min=\"0\" max=\"15\" id=\"intensity\" name=\"intensity\" size=\"2\"/>\n\
				</li>\n\
				<li>\n\
					<label for=\"amode\">Fade in - out mode</label>\n\
				</li>\n\
				<li>\n\
					<select id=\"amode\" name=\"amode\"/>\n\
						<option value=\"0\"%s>Don't fade the message</option>\n\
						<option value=\"1\"%s>From 0 to the intensity value</option>\n\
						<option value=\"2\"%s>From the intensity value to 0</option>\n\
						<option value=\"3\"%s>From 0 to the intensity value and back</option>\n\
						<option value=\"4\"%s>From the intensity value to 0 and back</option>\n\
					</select>\n\
				</li>\n\
				<li>\n\
					<label for=\"smode\">Scroll direction</label>\n\
				</li>\n\
				<li>\n\
					<select id=\"smode\" name=\"smode\"/>\n\
						<option value=\"0\"%s>No scroll</option>\n\
						<option value=\"1\"%s>Left to right</option>\n\
						<option value=\"2\"%s>Bottom to top</option>\n\
						<option value=\"3\"%s>Top to bottom</option>\n\
					</select>\n\
				</li>\n\
				<li>\n\
					<label for=\"msdelay\">Delay (ms)</label>\n\
				</li>\n\
				<li>\n\
					<input required value=\"%d\" placeholder=\"delay (ms)\" type=\"number\" min=\"15\" id=\"msdelay\" name=\"msdelay\" size=\"4\"/>\n\
				</li>\n\
				<li>\n\
					<label for=\"msg\">Message</label>\n\
				</li>\n\
				<li>\n\
					<input required tabindex=\"0\" autocomplete=\"on\" placeholder=\"Press ENTER to send\" id=\"msg\" name=\"msg\" maxlength=\"256\" autofocus ></textarea>\n\
				</li>\n\
				<li>\n\
					<h5>%s</h5>\n\
				</li>\n\
				<li>\n\
					<button type=\"submit\">Queue</button>\n\
				</li>\n\
			</ul>\n\
		</form>\n\
	</body>\n\
</html>\n\
"

#define O(m) options[m][0], options[m][1], options[m][2], options[m][3]
#define A(m) options[m][0], options[m][1], options[m][2], options[m][3], options[m][4]
// Form scroll mode select arrays
const unsigned char* options[5][5] = {
	{ "selected",  "", "", "", "" },
	{ "", "selected",  "", "", "" },
	{ "", "", "selected",  "", "" },
	{ "", "", "", "selected", "" },
	{ "", "", "", "", "selected" },
};

// For max7219mod_load_font debugging
bool _debug = false;

static unsigned char page[4096];

struct Message {
	unsigned char* message;
	unsigned int msdelay;
	unsigned int smode;
	unsigned int amode;
	unsigned int intensity;
	bool nocontent;
};

void displayMessage(const struct Message *m) {
	max7219mod_open();
	max7219mod_set_delay(m->msdelay);
	max7219mod_set_scroll_mode(m->smode);
	max7219mod_set_intensity(m->intensity);
	max7219mod_set_anim_mode(m->amode);
	max7219mod_send_string(m->message);
	max7219mod_close();
}

static int handle_method_not_allowed(struct MHD_Connection *conn) {
	struct MHD_Response *resp =
	    MHD_create_response_from_buffer(0, "", MHD_RESPMEM_PERSISTENT);
	int ret = MHD_queue_response(conn, MHD_HTTP_METHOD_NOT_ALLOWED, resp);
	MHD_destroy_response(resp);
	return ret;
}

const char* valueKind(enum MHD_ValueKind kind) {
	switch(kind) {
  	case MHD_HEADER_KIND: return "header";
  	case MHD_COOKIE_KIND: return "cookie";
		case MHD_POSTDATA_KIND: return "post data";
		case MHD_GET_ARGUMENT_KIND: return "get argument";
		case MHD_FOOTER_KIND: return "footer";
		default: return "unknown";
	}
}

static int headers_iterator(void *cls, enum MHD_ValueKind kind, const char *key,
		const char *value) {
	if(0 == strcasecmp(key, "x-no-content")) {
		struct Message* msg = (struct Message*)cls;
		//printf("Header X-No-Content present: not returning form\n");
		msg->nocontent = true;
	}
	return MHD_YES;
}

/**
 * Iterator over key-value pairs where the value
 * maybe made available in increments and/or may
 * not be zero-terminated.  Used for processing
 * POST data.
 *
 * @param cls user-specified closure
 * @param kind type of the value
 * @param key 0-terminated key for the value
 * @param filename name of the uploaded file, NULL if not known
 * @param content_type mime-type of the data, NULL if not known
 * @param transfer_encoding encoding of the data, NULL if not known
 * @param data pointer to size bytes of data at the specified offset
 * @param off offset of data in the overall value
 * @param size number of bytes in data available
 * @return MHD_YES to continue iterating,
 *         MHD_NO to abort the iteration
 */
static int
post_iterator (void *cls,
               enum MHD_ValueKind kind,
               const char *key,
               const char *filename,
               const char *content_type,
               const char *transfer_encoding,
               const char *data, uint64_t off, size_t size)
{
  (void) kind;              /* Unused. Silent compiler warning. */
  (void) filename;          /* Unused. Silent compiler warning. */
  (void) content_type;      /* Unused. Silent compiler warning. */
  (void) transfer_encoding; /* Unused. Silent compiler warning. */

	// Processor is called with empty string on last call
	if(0 == strcmp("", data)) return MHD_NO;

  if (0 == strcmp("msg", key)) {
  	//printf("Kind    : %s\n", valueKind(kind));
		//printf("Key     : %s\n", key);
		//printf("Data    : %s\n", data);
		struct Message* msg = (struct Message*)cls;
		int len = strlen(data) + 1;
		msg->message = malloc(sizeof(unsigned char) * len);
		strncpy(msg->message, data, len);
		return MHD_YES;
	}
  if (0 == strcmp ("msdelay", key)) {
		struct Message* msg = (struct Message*)cls;
		msg->msdelay = atoi(data);
		return MHD_YES;
	}
  if (0 == strcmp ("smode", key)) {
		struct Message* msg = (struct Message*)cls;
		msg->smode = atoi(data);
		return MHD_YES;
	}
  if (0 == strcmp ("intensity", key)) {
		struct Message* msg = (struct Message*)cls;
		msg->intensity = atoi(data);
		return MHD_YES;
	}
  if (0 == strcmp ("amode", key)) {
		struct Message* msg = (struct Message*)cls;
		msg->amode = atoi(data);
		return MHD_YES;
	}

	return MHD_NO;
}

static int ahc_echo(void *cls, struct MHD_Connection *conn, const char *url,
		const char *method, const char *version, const char *updata,
		size_t *updata_size, void **ptr) {
	static int dummy;
	static struct Message msg;
	struct MHD_Response *response;
 	struct MHD_PostProcessor *pp = *ptr;
	int ret = MHD_YES;
	unsigned char buf[21];
	time_t now;

	if(0 == strcmp(method, "GET")) {
		/* The first time only the headers are valid, do not respond yet... */
		if(&dummy != *ptr) {
			*ptr = &dummy;
			return MHD_YES;
		}
		*ptr = NULL;	/* clear context pointer */
		snprintf(page, 4096, cls, 1, A(0), O(1), 15, "");
		response = MHD_create_response_from_buffer(strlen(page), (void *)page,
			MHD_RESPMEM_PERSISTENT);
		ret = MHD_queue_response(conn, MHD_HTTP_OK, response);
		MHD_destroy_response(response);
	} else if(0 != strcmp(method, "POST")) {
		return handle_method_not_allowed(conn);
	} else if(NULL == pp) {
		msg.nocontent = false;
		pp = MHD_create_post_processor(conn, 1024, &post_iterator, &msg);
		if (NULL == pp) {
			fprintf (stderr, "Failed to setup post processor for `%s'\n", url);
			return MHD_NO;
		}
		*ptr = pp;
	} else if (*updata_size) {
		MHD_post_process(pp, updata, *updata_size);
		*updata_size = 0;
	} else {
		MHD_destroy_post_processor(pp);
		MHD_get_connection_values(conn, MHD_HEADER_KIND, &headers_iterator, &msg);
		if(NULL != msg.message && msg.message[0] != '\n') {
			if(msg.nocontent) snprintf(page, 4096, "");
			else snprintf(page, 4096, MAIN_PAGE, msg.intensity, A(msg.amode),
				O(msg.smode), msg.msdelay, "Message queued successfully");
			time(&now);
			strftime(buf, 20, "%Y/%m/%d %H:%M:%S", localtime(&now));
			printf("%s [%s], delay: %dms, dir: %d, int: %d, anim: %d\n", buf,
				msg.message, msg.msdelay, msg.smode, msg.intensity, msg.amode);
			displayMessage(&msg);
		} else {
			if(msg.nocontent) snprintf(page, 4096, "");
			else snprintf(page, 4096, MAIN_PAGE, msg.intensity, A(msg.amode),
				O(msg.smode), msg.msdelay, "<strong>Please, provide valid data</strong>");
		}
		response = MHD_create_response_from_buffer(strlen(page), (void *)page,
			MHD_RESPMEM_PERSISTENT);
		ret = MHD_queue_response(conn, MHD_HTTP_OK, response);
		MHD_destroy_response(response);
		free(msg.message);
		msg.message = NULL; // Very important
	}

	return ret;
}

int main(int argc, char **argv) {
	struct MHD_Daemon *d;
	if(argc != 2) {
		printf("%s PORT\n", argv[0]);
		return 1;
	}
	d = MHD_start_daemon(MHD_USE_THREAD_PER_CONNECTION,
			      atoi(argv[1]), NULL, NULL, &ahc_echo, MAIN_PAGE,
			     	MHD_OPTION_LISTENING_ADDRESS_REUSE, 1,
			     	MHD_OPTION_END);
	if(d == NULL) {
		perror("MHD_start_daemon");
		return 1;
	}
	max7219mod_load_font("fonts/iso01a-8x8.psf.gz");
	(void)getc(stdin);
	MHD_stop_daemon(d);
	return 0;
}
