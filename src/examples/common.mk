CHOST?=arm-linux-gnueabihf
CC:=$(if $(CHOST),$(CHOST)-gcc,$(CC))
ifeq ($(shell $(CC) -dumpversion 2>/dev/null),)
all:
	$(error `$(CC)' not found - please, install an ARMv7 toolchain >= 4.9.4)
else
require_flags = $(if $(shell pkg-config --with-path=$1 --libs $2 2>/dev/null),$(shell pkg-config --with-path=$1 --static --cflags --libs $2 2>/dev/null),$(error Requirement `$(2)' not found in $(1) - please, build the requirement before this program))
max7219m_PC = ../../max7219m/share/pkgconfig
LDFLAGS = -Wl,--allow-shlib-undefined
REQUIRED_LIBS += max7219m
ifeq ($(MAKECMDGOALS),)
all: CFLAGS := $(foreach lib,$(REQUIRED_LIBS),$(call require_flags,$($(lib)_PC),$(lib)))
all: $T
endif
ifeq ($(L),static)
%: %.c
	$(CC) $(CFLAGS) $(LDFLAGS) $< -static -pthread -l:libmicrohttpd.a -l:libmax7219m.a -l:libz.a -o $@
	$(CHOST)-strip $@
endif
endif

clean:
	rm -f $T *.o
install-%:
	scp $T root@$*:
